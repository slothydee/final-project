const validateStringLength = function(userInputString, minimumLength) {
    return userInputString >= minimumLength
}
const usedCategoriesValidation = function(usedCategoriesHTMLCollection) {
    let invalid = false
    const usedCategories = Array.from(usedCategoriesHTMLCollection)
    usedCategories.forEach(category => {
        let i = 1
        while (i < 4) {
            let inputId = `${category.id}${i}`
            let input = category.children[`${inputId}`] 
            if ((category.id !== 'kids' && !validateStringLength(input.value.length, 4)) 
            || (category.id === 'kids' && !validateStringLength(input.value.length, 1))) {
                input.classList.toggle('invalid') //todo message?
                invalid = true
            }
            else {
                input.classList.remove('invalid')
            }
            i ++
          }
    })
    return invalid
}

const userName = document.getElementById('name');
const form = document.getElementById('mash-form')
const fortuneCategories = document.getElementById('fortune-kinds')
const jobTitle = document.getElementById('partner-additional-fields')

userName.value = localStorage.getItem('savedName')

const additionalFields = function(selectedItemsArray) {
    let possibleCategories = ['partner', 'kids', 'occupation', 'city']
    const chosenCategories = possibleCategories.filter(value => selectedItemsArray.includes(value));
    const unchosenCategories = possibleCategories.filter(x => selectedItemsArray.indexOf(x) === -1)
    unchosenCategories.forEach(category => document.getElementById(category).classList = 'hidden')
    chosenCategories.forEach(category => {
        document.getElementById(category).classList = 'visible'
    })
}
fortuneCategories.addEventListener('change',function(e) {
    let $target = $(e.target)
    const selectedOptions = Array.from($target[0].selectedOptions)
    const selectedItems = []
    selectedOptions.forEach(item => selectedItems.push(item.value))
    additionalFields(selectedItems)
  })
form.addEventListener('submit', function(e) {
    e.preventDefault();
    const visibleItems = document.getElementsByClassName('visible')
    if (visibleItems != null) {
        if (usedCategoriesValidation(visibleItems)) {
            e.preventDefault()
        }
      };
    if (!validateStringLength(userName.value.length, 4)) { 
        userName.classList.toggle('invalid')
        e.preventDefault()
    }
    localStorage.setItem('savedName',userName.value)
    const letsFortuneTell = new FortuneTell(visibleItems, userName.value)

    letsFortuneTell.instantiate()
    setTimeout(function() { 
        letsFortuneTell.predict();
        letsFortuneTell.tellFortune()
    },
    3000
    );
  },true)

class FortuneTell {
    constructor(CategoryInputs, fortunePersonName) {
        this.CategoryInputs= CategoryInputs;
        this.mash = ['mansion', 'apartment', 'shack', 'house']
        this.fortunePersonName = fortunePersonName
    };
    instantiate() {
        const{CategoryInputs} = this;
        const arrayCats = Array.from(CategoryInputs)
        arrayCats.forEach(category =>{
            category['numRemaining'] = 3
        })
    }
    predict() {
        Array.from(this.CategoryInputs).forEach(category => {
            let i = 1
            const toKeep = Math.ceil(Math.random()*3);
            while (i < 4) {
                if (toKeep !== i) {
                    category.children[`${category.id}${i}`].classList = 'hidden'
                }
                i ++
            }
            })
        const toKeepMash = Math.floor(Math.random()*4);
        this.mash = this.mash[toKeepMash]
    }
    tellFortune() {
        let fortuneString = `I can see it now, ${this.fortunePersonName}...\nYou will live in a ${this.mash}...`
        Array.from(this.CategoryInputs).forEach(category => {
            let i = 1
            while (i < 4) {
                const childElement = category.children[`${category.id}${i}`]
                if (childElement.classList != 'hidden') {
                    if (category.id !== 'kids') {
                        fortuneString = `${fortuneString} Your ${category.id} will be ${childElement.value}...`
                    }
                    else {
                        fortuneString = `${fortuneString} You will have ${childElement.value} children...`
                    }
                }
                i ++
            }
            })
        document.querySelector('#fortune').classList = 'visible'
        document.querySelector('#fortune').innerHTML = fortuneString
    }
};


